#!/bin/bash
# double chiffrement du fichier log passé en paramètre ... ou via un pipe
# cf https://sud-ouest2.org/wiki/double_crypto_des_logs
# root a certifie les cles de destination
# (c) 2017 - Eric Seigne <eric.seigne@abul.org>
# GNU/GPL v3 ou plus

LOG=${1}
TEMPNAME=`tempfile`
rm -f ${TEMPNAME}

if [ -f "${LOG}" -a ! -z "${LOG}" ]; then
    echo "on essaye de chiffrer ${LOG} ..." >/dev/stderr
    gpg --sign --encrypt --batch -a -r admin-crypto-01@sud-ouest2.org -o ${TEMPNAME} ${LOG} 
    if [ $? == 0 ]; then
	gpg --sign --encrypt --batch -r admin-crypto-02@sud-ouest2.org ${TEMPNAME}
	if [ $? == 0 ]; then
	    mv "${TEMPNAME}" "${LOG}.gpg"
	    echo "Le fichier est pret : ${LOG}.gpg" >/dev/stderr
	else
	    echo "Erreur lors de la 2° passe" >/dev/stderr
	fi
    else
	echo "Erreur lors de la 1ere passe" >/dev/stderr
    fi
else
    #echo "No file passed ad first arg, trying stdin" >/dev/stderr
    gpg --sign --encrypt --batch -a -r admin-crypto-01@sud-ouest2.org -o ${TEMPNAME} -
    if [ $? == 0 ]; then
	gpg --sign --encrypt --batch -r admin-crypto-02@sud-ouest2.org -o - ${TEMPNAME}
	if [ $? == 0 ]; then
	    #echo "Le fichier est pret : stdout" >/dev/stderr
	    true
	else
	    echo "Erreur lors de la 2° passe" >/dev/stderr
	    exit -1
	fi
    else
	echo "Erreur lors de la 1ere passe" >/dev/stderr
	exit -1
    fi
fi

rm -f ${TEMPNAME}
