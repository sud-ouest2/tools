# Outils communs à tous les serveurs

## Double crypto des logs

La double crypto des logs est documentée sur le wiki : https://sud-ouest2.org/wiki/double_crypto_des_logs

Cf soo2-logs_crypt.sh 

## Sauvegardes des logs vers FTP

Les logs cryptés peuvent être expédiés vers un FTP tiers (par exemple celui que OVH met à dispo de tout serveur dédié loué chez eux)

L'automatisation est assurée par le script soo2-external_backup.sh 