#!/bin/bash
#push backup to FTP (OVH)
# (c) 2017 - Eric Seigne <eric.seigne@abul.org>
# GNU/GPL v3 ou plus

#fichier de configuration à faire dans /etc/soo2/config
#ID=loginFTP
#MDP=passFTP
#SRV=serveurFTP
#MYNAME=hostname
#LOGDIR=/var/log

. /etc/soo2/config

if [ ! -x /usr/bin/lftp ]; then
    echo "ERROR: lftp not found !"
    exit -1
fi

#on ne veut surtout pas exporter les fichiers non cryptés (serveur FTP peu sûr)
EXCLUDE=""
cd ${LOGDIR}
for filename in `find . ! -name "*.gpg"`
do
    name=`echo ${filename} | sed s%"^./"%""%`
    if [ -f "${filename}" ]; then
	EXCLUDE+="--exclude ${name} "
    fi
done

lftp -c "set ftp:list-options -a;
set ssl:check-hostname no;
open ftp://${ID}:${MDP}@${SRV}; 
lcd ${LOGDIR};
mkdir -f -p ${MYNAME}${LOGDIR};
cd ${MYNAME}${LOGDIR};
mirror --reverse --delete --use-cache --verbose --allow-chown --allow-suid --no-umask --parallel=2 ${EXCLUDE} --include-glob *.gpg"

